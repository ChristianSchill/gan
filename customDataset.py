import os
import pandas as pd
import torch
from torch.utils.data import Dataset
from PIL import Image



class MyDataset(Dataset):
    def __init__(self, txt_file, root_dir, transform=None):
        with open(txt_file) as f:
            self.images = f.readlines()
        self.images = [x.strip() for x in self.images]
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        img_path = os.path.join(self.root_dir, self.images[index])
        image = Image.open(img_path)
        if self.transform:
            image = self.transform(image)

        return image, 1

